### LOOK INTO
Typical gotcha's 
- Fall into cases where you are summarazing and not analyzing results. Give a reason why you are getting the results you are.
- Results need to be the foundation for your analysis
- Use results to compare and contrast algos.

# Project 1
# What to turn in
- [ ] a file named README.txt containing instructions for running your code (see note below)
```md
Note below: we need to be able to get to your code and your data. Given what you're turning in, you will want to arrange for an URL of some sort. Having said that, providing all of Weka isn't necessary, for example, because I can get it myself; however, you should at least provide any files you found necessary to change and enough support and explanation so we could reproduce your results if we really wanted to do so. In any case, include all the information in README.txt
```
- [ ] a file named yourgtaccount-analysis.pdf containing your writeup (GT account is what you log in with, not your all-digits ID)
```md
The file yourgtaccount-analysis.pdf should contain:

* a description of your classification problems, and why you feel that they are interesting. Think hard about this. To be at all interesting the problems should be non-trivial on the one hand, but capable of admitting comparisons and analysis of the various algorithms on the other. Avoid the mistake of working on the largest most complicated and messy dataset you can find. The key is to be interesting and clear, no points for hairy and complex.
* the training and testing error rates you obtained running the various learning algorithms on your problems. At the very least you should include graphs that show performance on both training and test data as a function of training size (note that this implies that you need to design a classification problem that has more than a trivial amount of data) and--for the algorithms that are iterative--training times/iterations. Both of these kinds of graphs are referred to as learning curves, BTW.
* analyses of your results. Why did you get the results you did? Compare and contrast the different algorithms. What sort of changes might you make to each of those algorithms to improve performance? How fast were they in terms of wall clock time? Iterations? Would cross validation help (and if it would, why didn't you implement it?)? How much performance was due to the problems you chose? How about the values you choose for learning rates, stopping criteria, pruning methods, and so forth (and why doesn't your analysis show results for the different values you chose? Please do look at more than one. And please make sure you understand it, it only counts if the results are meaningful)? Which algorithm performed best? How do you define best? Be creative and think of as many questions you can, and as many answers as you can but a lot of the questions boil down to: why... WHY WHY WHY?
```

## Environment Set Up
Install miniconda3
With miniconda installed, install the packages necessary to run Scikit-learn and generate the plots for the report.
Run
```
conda create -n ml_env2 -c conda-forge scikit-learn=1.2.1 pandas matplotlib
```
This should install scikit version 1.2, numpy, scipy, and a number of other packages for you to work on in a dedicated environment

## Running The Code

Refer to the 'Gather Code' section to gain access to the code for this project

Download the Mushroom data set csv from https://www.kaggle.com/code/aavigan/uci-mushroom-data/data and place it in a folder called input that resides in the same directory as your Suprveised_Learning.py file

Modify the variable output_file_path to point to where you would like your plot images to export to.

To get our default model plots set tune_model to False, and set_hyper_params to True.

To get our tuning plots set set_hyper_params to True. If you are trying to get the pruning plots for decision tree and boosting, set prune_decision_tree to True.

Run the code.

After running, change set set_hyper_params to False, and the program will go through generate the learning curves for our tuned models, print our timing information, confusion matrix, and classification reports.

Plots should be output where you designated, while tabular information will need to be pulled from your execution logs.

## Gather Code 
There are two ways to gather code.
either git clone the following link:
https://gitlab.com/oms-gatech/machine-learning/projects/supervised-learning.git

The code resides in Supervised_Learning.py
All other files in this project are for generating the associated PDF using Latex.

or Copy the following lines into your IDE.
```python
# Large portions of this code was taken from an example implemetation on https://www.kaggle.com/code/aavigan/uci-mushroom-data/notebook
# Small modifications were made to add desired graphs, add functionality, hyper parameter tunings, and to remove unwanted functionality.
# The documentation at https://scikit-learn.org/stable/index.html was also referenced heavily throughout the following project

#import modules
# import graphviz
import pandas as pd
import pathlib
import copy
import numpy as np
import time
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE
from sklearn.model_selection import train_test_split, KFold, learning_curve, LearningCurveDisplay, cross_val_score, validation_curve
from sklearn.metrics import confusion_matrix, classification_report
from statistics import mean
from sklearn.tree import export_graphviz
from sklearn.datasets import fetch_openml
from sklearn.tree import DecisionTreeClassifier #Pruning, Max Depth
from sklearn.neural_network import MLPClassifier #Hidden Layers, Learning random_state
from sklearn.ensemble import AdaBoostClassifier # Weak Learner
from sklearn.neighbors import KNeighborsClassifier #k
from sklearn.svm import NuSVC #kernel type
from sklearn.exceptions import ConvergenceWarning

set_hyper_params = True
prune_decision_tree = True
tune_model = True
output_file_path = "C:\\Users\\lband\\Documents\\gatech\\CS 7641 - Machine Learning\\Projects\\supervised-learning\\Documentation\\images\\"

def get_models():
    models = list()
    models.append(DecisionTreeClassifier(random_state=42))
    models.append(MLPClassifier(random_state=42))
    models.append(KNeighborsClassifier(algorithm="kd_tree"))
    models.append(AdaBoostClassifier(estimator=DecisionTreeClassifier() ,random_state=42))
    models.append(NuSVC(random_state=42))
    return models

def get_data(data_title):
    if(data_title == 'mushroom'):
        mushrooms = pd.read_csv(str(pathlib.Path(__file__).parent.resolve()) + "\input\mushrooms.csv")
        #create dummy variables
        mushrooms = pd.get_dummies(mushrooms)
        #subset data into dependent and independent variables x,y
        LABELS = ['class_e', 'class_p']
        FEATURES = [a  for a in mushrooms.columns if a not in LABELS ]
        y = mushrooms[LABELS[0]]
        x = mushrooms[FEATURES]
    else: 
        data = fetch_openml(name=data_title, parser='auto')
        y = data.target
        x = data.data
    X_train, X_test, y_train, y_test = train_test_split(x, y, test_size = 0.2, random_state = 42)
    return X_train, y_train, X_test, y_test

def evaluate_model(cv, model, data_title):
    X_train, y_train, X_test, y_test = get_data(data_title)
    if(model.__class__.__qualname__ == "DecisionTreeClassifier" and tune_model):
        if(prune_decision_tree):
            path = model.cost_complexity_pruning_path(X_train, y_train)
            ccp_alphas, impurities = path.ccp_alphas, path.impurities
            clfs = []
            for ccp_alpha in ccp_alphas:
                model.ccp_alpha=ccp_alpha
                clf = copy.deepcopy(model)
                # path = tree.cost_complexity_pruning_path(train_X, train_y)
                clf.fit(X_train, y_train)
                clfs.append(clf)
            print(
                "Number of nodes in the last tree is: {} with ccp_alpha: {}".format(
                    clfs[-1].tree_.node_count, ccp_alphas[-1]
                )
            )
            train_scores, valid_scores = validation_curve(model, X_train, y_train, param_name="ccp_alpha", param_range=ccp_alphas[:-1], cv=cv)
            mean_train = []
            mean_valid = []
            for score in valid_scores:
                mean_valid.append(mean(score))
            for score in train_scores:
                mean_train.append(mean(score))
            clfs = clfs[:-1]
            ccp_alphas = ccp_alphas[:-1]
            node_counts = [clf.tree_.node_count for clf in clfs]
            depth = [clf.tree_.max_depth for clf in clfs]
            fig, ax = plt.subplots(3, 1)
            ax[0].plot(ccp_alphas, node_counts, marker="o", drawstyle="steps-post")
            ax[0].set_xlabel("alpha")
            ax[0].set_ylabel("number of nodes")
            ax[0].set_title("Number of nodes vs alpha")
            ax[1].plot(ccp_alphas, depth, marker="o", drawstyle="steps-post")
            ax[1].set_xlabel("alpha")
            ax[1].set_ylabel("depth of tree")
            ax[1].set_title("Depth vs alpha")
            ax[2].plot(ccp_alphas, mean_train, marker="o", drawstyle="steps-post", label="Train Scores")
            ax[2].plot(ccp_alphas, mean_valid, marker="o", drawstyle="steps-post", label="Validation Scores")
            ax[2].legend()
            ax[2].set_xlabel("alpha")
            ax[2].set_ylabel("Score")
            ax[2].set_title("Score vs alpha")
            plt.savefig(str(output_file_path + "Decision Tree\\" + "ccp_pruning_" + data_title + ".png"))
            plt.clf()
            fig.tight_layout()
        else: #Max Depth Plotting
            Depths = [30, 25, 20, 15, 10, 7, 6, 5, 4, 3, 2]
            train_scores, valid_scores = validation_curve(model, X_train, y_train, param_name="max_depth", param_range=Depths, cv=cv)
            mean_train = []
            mean_valid = []
            for score in valid_scores:
                mean_valid.append(mean(score))
            for score in train_scores:
                mean_train.append(mean(score))
            fig, ax = plt.subplots()
            ax.plot(Depths, mean_train, marker="o", drawstyle="steps-post", label="Train Scores")
            ax.plot(Depths, mean_valid, marker="o", drawstyle="steps-post", label="Validation Scores")
            ax.legend()
            ax.set_xlabel("Depth")
            ax.set_ylabel("Score")
            ax.set_title(data_title + " Score vs Depth")
            fig.tight_layout()
            plt.savefig(str(output_file_path + "Decision Tree\\" + "max_depth_" + data_title + ".png"))
            plt.clf()

        scores = [0, 0, 0]
    elif(model.__class__.__qualname__ == "MLPClassifier" and tune_model):
        tune_param = ["hidden_layer_sizes", "learning_rate_init"] #
        if(data_title == "wdbc"):
            tune_range = [[(2000,), (1000,), (500,), (1000, 1000), (1000, 500), (26, 26, 26, 26, 26, 26, 26), (13,2)], [0.0001, 0.0005, 0.00075, 0.001, 0.005, 0.01, 0.02]] 
        else:
            tune_range = [[(1000,), (100,), (4,), (1, 2), (4, 2), (1, 1, 1, 1), (3,1)], [0.000025, 0.00005, 0.0001, 0.0005, 0.00075, 0.001, 0.005]]
        for temp_param in tune_param:
            model = MLPClassifier(random_state=42)
            if(temp_param == "hidden_layer_sizes"):
                temp_range = tune_range[0]
            else:
                temp_range = tune_range[1]
            for field in temp_range:
                if(temp_param == "hidden_layer_sizes"):
                    model.set_params(hidden_layer_sizes = field)
                elif (temp_param == "learning_rate_init"):
                    model.set_params(learning_rate_init = field)                
                model.fit(X_train, y_train)
                plt.plot(model.loss_curve_, label= field)
            plt.legend()
            plt.xlabel("Iterations")
            plt.xlim(right=100)
            plt.ylabel("Loss")
            plt.title("Loss curves for " + temp_param + " in " + data_title + " data set")
            plt.savefig(str(output_file_path + "Neural Network\\" + data_title + "Losscurvesfor_" + temp_param + ".png"))
            plt.clf()
            model = MLPClassifier(random_state=42)
            train_scores, valid_scores = validation_curve(model, X_train, y_train, param_name=temp_param, param_range=temp_range, cv=cv)
            mean_train = []
            mean_valid = []
            for score in valid_scores:
                mean_valid.append(mean(score))
            for score in train_scores:
                mean_train.append(mean(score))
            str_range = []
            for value in temp_range:
                if(value == (26, 26, 26, 26, 26, 26, 26)):
                    str_range.append('26 x 7')
                else:
                    str_range.append(str(value))
            plt.figure(tight_layout=True)
            plt.plot(str_range, mean_train, marker="o", drawstyle="steps-post", label="Train Scores",)
            plt.plot(str_range, mean_valid, marker="o", drawstyle="steps-post", label="Validation Scores")
            plt.legend()
            plt.xlabel(temp_param)
            plt.ylabel("Score")
            plt.title("Score vs " + temp_param + " of " + data_title)
            plt.savefig(str(output_file_path+ "Neural Network\\" + data_title + "Score_vs_" + temp_param + ".png"))
            plt.clf()
        scores = [0,0,0]
    elif(model.__class__.__qualname__ == "KNeighborsClassifier" and tune_model):
        tune_param = ["n_neighbors", "weights"] #
        # if(data_title == "wdbc"):
        tune_range = [[20, 10, 7, 5, 2, 1], ["uniform", "distance"]]# [5, 10, 15, 30, 40, 80, 160]] 
        # else:
        #     tune_range = [[(1000,), (100,), (4,), (1, 2), (4, 2), (1, 1, 1, 1), (3,1)], [0.000025, 0.00005, 0.0001, 0.0005, 0.00075, 0.001, 0.005]]
        for temp_param in tune_param:
            if(temp_param == "n_neighbors"):
                temp_range = tune_range[0]
            else:
                temp_range = tune_range[1]
            model = KNeighborsClassifier(algorithm="kd_tree")
            train_scores, valid_scores = validation_curve(model, X_train, y_train, param_name=temp_param, param_range=temp_range, cv=cv)
            mean_train = []
            mean_valid = []
            for score in valid_scores:
                mean_valid.append(mean(score))
            for score in train_scores:
                mean_train.append(mean(score))
            str_range = []
            for value in temp_range:
                    str_range.append(str(value))
            plt.figure(tight_layout=True)
            plt.plot(str_range, mean_train, marker="o", drawstyle="steps-post", label="Train Scores",)
            plt.plot(str_range, mean_valid, marker="o", drawstyle="steps-post", label="Validation Scores")
            plt.legend()
            plt.xlabel(temp_param)
            plt.ylabel("Score")
            plt.title("Score vs " + temp_param + " of " + data_title)
            plt.savefig(str(output_file_path + "KNN\\" + data_title + "Score_vs_" + temp_param + ".png"))
            plt.clf()
        scores = [0,0,0]
    elif(model.__class__.__qualname__ == "AdaBoostClassifier" and tune_model):
        if(prune_decision_tree):
            estimator_model = DecisionTreeClassifier(random_state=42, max_depth = 7)
            path = estimator_model.cost_complexity_pruning_path(X_train, y_train)
            ccp_alphas, impurities = path.ccp_alphas, path.impurities
            clfs = []
            for ccp_alpha in ccp_alphas:
                estimator_model.ccp_alpha=ccp_alpha
                model = AdaBoostClassifier(estimator_model, random_state=42)
                clf = copy.deepcopy(model)
                # path = tree.cost_complexity_pruning_path(train_X, train_y)
                clf.fit(X_train, y_train)
                clfs.append(clf)
            print(
                "Number of nodes in the last tree is: {} with ccp_alpha: {}".format(
                    clfs[-1].estimators_[-1].tree_.node_count, ccp_alphas[-1]
                )
            )
            train_scores, valid_scores = validation_curve(model.estimator, X_train, y_train, param_name="ccp_alpha", param_range=ccp_alphas[:-1], cv=cv)
            mean_train = []
            mean_valid = []
            for score in valid_scores:
                mean_valid.append(mean(score))
            for score in train_scores:
                mean_train.append(mean(score))
            clfs = clfs[:-1]
            ccp_alphas = ccp_alphas[:-1]
            node_counts = [clf.estimators_[-1].tree_.node_count for clf in clfs]
            depth = [clf.estimators_[-1].tree_.max_depth for clf in clfs]
            fig, ax = plt.subplots(3, 1)
            ax[0].plot(ccp_alphas, node_counts, marker="o", drawstyle="steps-post")
            ax[0].set_xlabel("alpha")
            ax[0].set_ylabel("number of nodes")
            ax[0].set_title("Number of nodes vs alpha")
            ax[1].plot(ccp_alphas, depth, marker="o", drawstyle="steps-post")
            ax[1].set_xlabel("alpha")
            ax[1].set_ylabel("depth of tree")
            ax[1].set_title("Depth vs alpha")
            ax[2].plot(ccp_alphas, mean_train, marker="o", drawstyle="steps-post", label="Train Scores")
            ax[2].plot(ccp_alphas, mean_valid, marker="o", drawstyle="steps-post", label="Validation Scores")
            ax[2].legend()
            ax[2].set_xlabel("alpha")
            ax[2].set_ylabel("Score")
            ax[2].set_title("Score vs alpha")
            fig.tight_layout()
            plt.savefig(str(output_file_path + "Boost\\" + "Score vs Alpha ccp_pruning_" + data_title + ".png"))
            plt.clf()
        else:
            number_of_estimators = [1, 5, 15, 25, 50, 75, 100, 150, 300]
            train_scores, valid_scores = validation_curve(model, X_train, y_train, param_name="n_estimators", param_range=number_of_estimators, cv=cv)
            mean_train = []
            mean_valid = []
            for score in valid_scores:
                mean_valid.append(mean(score))
            for score in train_scores:
                mean_train.append(mean(score))
            fig, ax = plt.subplots()
            ax.plot(Depths, mean_train, marker="o", drawstyle="steps-post", label="Train Scores")
            ax.plot(Depths, mean_valid, marker="o", drawstyle="steps-post", label="Validation Scores")
            ax.legend()
            ax.set_xlabel("n_Estimators")
            ax.set_ylabel("Score")
            ax.set_title(data_title + " Score vs Depth")
            fig.tight_layout()
            plt.savefig(str(output_file_path + "Boost\\" + "Score vs n_Estimators" + data_title + ".png"))
            plt.clf()
        scores = [0, 0, 0]
    elif(model.__class__.__qualname__ == "NuSVC" and tune_model):
        tune_param = ["kernel", "nu"] #
        # # if(data_title == "wdbc"):
        tune_range = [["linear", "poly", "rbf", "sigmoid"], [0.1, 0.2, 0.3, 0.5, 0.6, 0.75, 1.0]] 
        # else:
        #     tune_range = [[(1000,), (100,), (4,), (1, 2), (4, 2), (1, 1, 1, 1), (3,1)], [0.000025, 0.00005, 0.0001, 0.0005, 0.00075, 0.001, 0.005]]
        for temp_param in tune_param:
            if(temp_param == "kernel"):
                temp_range = tune_range[0]
            else:
                temp_range = tune_range[1]
            train_scores, valid_scores = validation_curve(model, X_train, y_train, param_name=temp_param, param_range=temp_range, cv=cv)
            mean_train = []
            mean_valid = []
            for score in valid_scores:
                mean_valid.append(mean(score))
            for score in train_scores:
                mean_train.append(mean(score))
            str_range = []
            for value in temp_range:
                str_range.append(str(value))
            plt.figure(tight_layout=True)
            plt.plot(str_range, mean_train, marker="o", drawstyle="steps-post", label="Train Scores",)
            plt.plot(str_range, mean_valid, marker="o", drawstyle="steps-post", label="Validation Scores")
            plt.legend()
            plt.xlabel(temp_param)
            plt.ylabel("Score")
            plt.title("Score vs " + temp_param + " of " + data_title)
            plt.savefig(str(output_file_path + "SVM\\" + data_title + "Score_vs_" + temp_param + ".png"))
            plt.clf()
        scores = [0,0,0]
    else:
        train_sizes, train_scores, test_scores = learning_curve(model, X_train, y_train)
        display = LearningCurveDisplay.from_estimator(model, X_train, y_train, score_name="Score")
        display.ax_.set_title(model.__class__.__qualname__ + " Learning Curve for " + data_title)
        display.figure_.savefig(output_file_path + model.__class__.__qualname__ + " Learning Curve for " + data_title +".png")
        # path = tree.cost_complexity_pruning_path(train_X, train_y)
        scores = cross_val_score(model, X_train, y_train, scoring='accuracy', cv=cv, n_jobs=-1)
    # return scores
    return mean(scores)

def train_and_test_model(model, data_title):
    if(model.__class__.__qualname__ == "AdaBoostClassifier"):
        if(data_title == "wbdc"):
            temp_ccp_alpha = (0.0056)
            temp_n_estimators = (50)
        else:
            temp_ccp_alpha = (0.00092)
            temp_n_estimators = (25)
        estimator_model = DecisionTreeClassifier(random_state=42, max_depth = 7, ccp_alpha=temp_ccp_alpha)
        model = AdaBoostClassifier(estimator_model, random_state=42, n_estimators=temp_n_estimators)
    X_train, y_train, X_test, y_test = get_data(data_title)
    start = time.time_ns()
    #Pruning code stems from https://scikit-learn.org/stable/auto_examples/tree/plot_cost_complexity_pruning.html
    model.fit(X_train, y_train)
    #Run Trained model against test cases that have been witheld
    test_prediction = model.predict(X_test)
    print("Fit and Predict Time = " + str((time.time_ns() - start) * 0.000000001) + " seconds")
    print("Model Test Results " + data_title + " " + model.__class__.__qualname__)
    print(confusion_matrix(y_test, test_prediction))
    print(classification_report(y_test, test_prediction))
    train_sizes, train_scores, test_scores = learning_curve(model, X_train, y_train)
    display = LearningCurveDisplay.from_estimator(model, X_train, y_train, score_name="Score")
    display.ax_.set_title(model.__class__.__qualname__ + " Learning Curve for " + data_title)
    display.figure_.savefig(output_file_path + model.__class__.__qualname__ + " Tuned Learning Curve for " + data_title +".png")

data_library = ["wdbc", "mushroom"]
#data_library = ["mushroom"]

for data_title in data_library:
    models = get_models()
    kf = KFold(n_splits=8, shuffle=True, random_state=42)
    for model in models:
        if(set_hyper_params):
            print(evaluate_model(kf, model, data_title))
        else:
            ##instantiate DecisionTreeClassifier:tree
            if(model.__class__.__qualname__ == "DecisionTreeClassifier"):
                #Set hyperparameters to optimal values found above.
                if(data_title == "wdbc"):
                    model.ccp_alpha = 0.0125
                    model.max_depth = 5
            elif(model.__class__.__qualname__ == "MLPClassifier"):
                #Set hyperparameters to optimal values found above.
                if(data_title == "wbdc"):
                    model.hidden_layer_sizes = (1000,)
                    model.learning_rate_init = (0.005)
                else:
                    model.hidden_layer_sizes = (4,)
                    model.learning_rate_init = (0.02)
            elif(model.__class__.__qualname__ == "KNeighborsClassifier"):
                model = KNeighborsClassifier(algorithm="kd_tree")
                #Set hyperparameters to optimal values found above.
                if(data_title == "wbdc"):
                    model.n_neighbors = 20
                    model.weights = "distance"
                else:
                    model.n_neighbors = 5
                    model.weights = "distance"
            elif(model.__class__.__qualname__ == "AdaBoostClassifier"):
                #Set hyperparameters to optimal values found above.
                if(data_title == "wbdc"):
                    model.n_estimators = (50)
                    # model.ccp_alpha = (0.0056)
                else:
                    model.n_estimators = (25)
                    # model.ccp_alpha = (0.00092)
            elif(model.__class__.__qualname__ == "NuSVC"):
                #Set hyperparameters to optimal values found above.
                if(data_title == "wbdc"):
                    model.kernel = "rbf"
                    model.nu = 0.05
                else:
                    model.kernel = "poly"
                    model.nu = 0.1

            train_and_test_model(model, data_title)


    #export a graphic representation of tree to file
```


